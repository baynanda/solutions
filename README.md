# Task 1
## Application and Solution Overview

This is a simple Hello World application using Python Flask. This is also include build and deployment solution from scratch to ready-for-service.

## Assumption
We will assume you have:
1. basic knowledge on Terraform
2. basic knowledge on Kubernetes
3. basic knowledge on Ansible

## Components
### Application
I am using python flask for my helloworld application. Using python we can choose which IP address and Port we want to listen for this Application. We also able to choose simple template for our web page, I choose HTML because I think HTML is good enough and also easy to use.

### Docker
Docker is a famous easy to use container-platform. I use Docker on my remote host. I define "DOCKER_HOST" variable to access my remote Docker host. I also use my docker hub account to store my container image, so my kubernetes cluster able to pull my image(s). Docker will package our application and also responsible to push our image.

### Kubernetes
Kubernetes is popular container-orchestration platform. We can deploy our containerized-application with easy, scalable and reliable. I use 'kubectl' to manage my resources on my Kubernetes cluster. I use Terraform and RKE to deploy my kubernetes cluster.

### Terraform
Terraform is Popular Infrastructure-as-a-Code (IaC) tools. It allow us to provision, manage and also delete our resources such as Virtual Machine, VPC, Pod and many more. I use Terraform to automate from build to deployment process. And we can use the same Terraform script for changed-code deployment. Terraform will integrate these automation processes.
- inform our docker runtime to packaging our application
- push our image
- deploy packaged-application (container) to our kubernetes cluster
- deploy supporting resources such as service and ingress

## Folder Structure
```sh
.
├── README.md --> explanation file
├── app --> application source code folder
│   └── v1 
│       ├── Dockerfile 
│       ├── app.py
│       ├── requirements.txt
│       └── templates 
│           └── index.html
├── kubernetes --> contains kubernetes file for testing on kubernetes cluster
│   ├── ingress.yaml
│   ├── kubeconfig.yaml
│   └── v1
│       └── web-deployment.yaml
├── main.tf --> terraform deployment file
└── module --> terraform module source folder
    ├── docker_build
    │   ├── main.tf
    │   ├── output.tf
    │   └── vars.tf
    ├── kubernetes_deploy 
    │   ├── main.tf
    │   └── vars.tf
    ├── kubernetes_ingress
    │   ├── main.tf
    │   └── vars.tf
    └── kubernetes_service
        ├── main.tf
        ├── output.tf
        └── vars.tf
```


## Preparation
1. Docker Host.
2. Kubernetes cluster.
3. Terraform installed.
4. Python3 runtime and Flask package installed.
5. Visual Code (optional).

## Deploy Application
1. Login to your Docker hub account using
    ```$ docker login ```
2. Go to  ```app/v1``` directory (optionally edit the source code as you desired).
3. Add required package for application to ```requirements.txt``` file.
4. Edit ```Dockerfile``` based on applicaton behaviour.
5. Build the application using ```$ docker build -t [image_name]:[tag] .```
5. Fill required parameter on ```main.tf``` based on you environment. 
6. Initialize you terrafrom script with ```$ terraform init```
7. Plan you terraform deployment with ```$ terraform plan```
8. Apply terraform deployment with ```$ terraform apply``` then accept the deployment by typing ```yes``` when prompted.

## Limitation
1. Unable to check the existing image and tag on container registry.
2. limited only one application version.
3. need manual login for docker.

## Potential Improvement
1. Integrate with docker for push image process.
2. Capability for Create Volume.
3. Create Blue/Green Deployment aproach.