terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.10.0"
    }
  }
}

provider "kubernetes" {
  config_path = "./kubernetes/kubeconfig.yaml" # kubeconfig path
}

# ================================== v1 ==================================
# build image
module "build_1" {
  source = "./module/docker_build"

  image_name = "baynanda/takehomeexam-web" # image name
  image_tag = "v1" # image tag
  docker_host = "ssh://root@10.1.101.174" # docker runtime location
  dockerfile_path = "app/v1" # docker file path
}

# push image to docker hub
resource "null_resource" "push_1" { 
 provisioner "local-exec" {
   command = "docker push ${module.build_1.image_name}:${module.build_1.image_tag}"
 }
 depends_on = [ module.build_1.image_tag ]
 triggers = {
   "image_tag" = module.build_1.image_tag
 }
}

# deploy container
module "deploy_1" {
  source = "./module/kubernetes_deploy/"
  depends_on = [
    null_resource.push_1
  ]
  
  deployment_name = "takehomeexam-v1-web-deployment" # deployment name
  app_name = "takehomeexam" # app name
  app_tier = "web" # app tier name
  app_version = "1" # app version
  replicas = "3" # replicas of pod
  image_name = module.build_1.image_name # image name will refer to build module
  image_tag = module.build_1.image_tag # image tag will refer to build module
  container_name = "takehomeexam-v1-web-container" # container name

  # resource allocation
  req_cpu = "100m"
  req_mem = "128Mi"
  limit_cpu = "1000m"
  limit_mem = "512Mi"
}

module "service_1" {
  source = "./module/kubernetes_service/"

  service_name = "takehomeexam-v1-web-service" # service name
  app_name = "takehomeexam" # selectopr label app
  app_tier = "web"
  app_version = "1"
  port = 80 # service port
  target_port = 8080 # target port 
  type = "ClusterIP"
}

module "ingress" {
  source = "./module/kubernetes_ingress"

  depends_on = [
    module.service_1
  ]

  ingress_name = "takehomeexam-ingress" # ingress name
  host = "takehome.exam"
  path = "/" # path
  service_name = module.service_1.service_name # target service name
  port = module.service_1.port # target service port
}