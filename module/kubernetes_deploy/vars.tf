variable "deployment_name" {
    type = string
}

variable "app_name" {
    type = string
}

variable "app_tier" {
    type = string
}

variable "app_version" {
    type = string
}

variable "replicas" {
    type = string
}

variable "image_name" {
    type = string
}

variable "image_tag" {
    type = string
}

variable "container_name" {
    type = string
}

variable "req_cpu" {
    type = string
}

variable "req_mem" {
    type = string
}

variable "limit_cpu" {
    type = string
}

variable "limit_mem" {
    type = string
}