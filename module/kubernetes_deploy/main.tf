resource "kubernetes_deployment_v1" "deploy" {
  metadata {
    name = var.deployment_name
    labels = {
      app = var.app_name
      tier = var.app_tier
      version = var.app_version
    }
  }

  spec {
    replicas = var.replicas

    selector {
      match_labels = {
        app = var.app_name
        tier = var.app_tier
        version = var.app_version
      }
    }

    template {
      metadata {
        labels = {
          app = var.app_name
          tier = var.app_tier
          version = var.app_version
        }
      }

      spec {
        container {
          image = "${var.image_name}:${var.image_tag}"
          name  = var.container_name

          resources {
            requests = {
              cpu    = var.req_cpu
              memory = var.req_mem
            }
            limits = {
              cpu    = var.limit_cpu
              memory = var.limit_mem
            }
          }
        }
      }
    }
  }
}
