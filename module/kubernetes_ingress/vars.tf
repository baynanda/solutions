variable "ingress_name" {
    type = string
}

variable "host" {
    type = string
}

variable "path" {
    type = string
}

variable "service_name" {
    type = string
}

variable "port" {
    type = string
}