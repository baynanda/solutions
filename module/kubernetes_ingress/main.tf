resource "kubernetes_ingress_v1" "example_ingress" {
  metadata {
    name = var.ingress_name
  }

  spec {
    rule {
      host = var.host
      http {
        path {
          path = var.path
          backend {
            service {
              name = var.service_name
              port {
                number = var.port
              }
            }
          }
        }
      }
    }
  }
}