variable "service_name" {
    type = string
}

variable "app_name" {
    type = string
}

variable "app_tier" {
    type = string
}

variable "app_version" {
    type = string
}

variable "port" {
    type = string
}

variable "target_port" {
    type = string
}

variable "type" {
    type = string
    default = "ClusterIP"
}