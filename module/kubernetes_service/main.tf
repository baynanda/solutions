resource "kubernetes_service_v1" "service" {
  metadata {
    name = var.service_name
  }
  spec {
    selector = {
      app = var.app_name
      tier = var.app_tier
      version = var.app_version
    }
    
    port {
      port        = var.port
      target_port = var.target_port
    }

    type = var.type
  }
}