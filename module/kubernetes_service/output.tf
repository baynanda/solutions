output "service_name" {
  value = kubernetes_service_v1.service.metadata[0].name
}

output "port" {
  value = kubernetes_service_v1.service.spec[0].port[0].port
}