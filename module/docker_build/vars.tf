variable "docker_host" {
    type = string
}

variable "image_name" {
    type = string
}

variable "image_tag" {
    type = string
}

variable "dockerfile_path" {
    type = string
}

