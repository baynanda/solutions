terraform {
  required_providers {
    docker = {
      source = "kreuzwerker/docker"
      version = "2.16.0"
    }
  }
}

provider "docker" {
  host = var.docker_host
}

resource "docker_image" "build" {
  name = var.image_name
  build {
    path = var.dockerfile_path
    tag  = ["${var.image_name}:${var.image_tag}"]
  }
}