output "image_name" {
    value = docker_image.build.name
}

output "image_tag" {
    value = var.image_tag
}